﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
        public class ProductosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Productos
        [HttpGet]
        public IEnumerable<Producto> GetProductos()
        {
            return _context.Productos.Include(x => x.TipoProducto).Include(x => x.Usuario);
        }

        // GET: api/Productos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProducto([FromRoute] int id, string id2)
        {
            if (!ModelState.IsValid)
            {
                // No tengo idea de si esto funcione, no se como agregar la segunda clave foranea
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Producto = await _context.Productos.Include(x => x.TipoProducto).Include(x => x.Usuario).SingleOrDefaultAsync(x => x.IdTipoProducto == id && x.IdUsuario == id2);

            if (Producto == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Producto.ArticuloSingular, Producto.NombreSingular) });
            }

            return Ok(Producto);
        }

        // PUT: api/Productos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProducto([FromRoute] int id, [FromBody] Producto Producto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != Producto.IdTipoProducto)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(Producto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!ProductoExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Producto.ArticuloSingular, Producto.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/Productos
        [HttpPost]
        public async Task<IActionResult> PostProducto([FromBody] Producto Producto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.Productos.Add(Producto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/Productos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var Producto = await _context.Productos.FindAsync(id);
            if (Producto == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Producto.ArticuloSingular, Producto.NombreSingular) });

            }

            _context.Productos.Remove(Producto);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool ProductoExists(int id)
        {
            return _context.Productos.Any(e => e.IdTipoProducto == id);
        }
    }

}