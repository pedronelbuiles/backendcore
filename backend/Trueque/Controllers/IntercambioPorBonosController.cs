﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class IntercambioPorBonosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public IntercambioPorBonosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/IntercambioPorBonos
        [HttpGet]
        public IEnumerable<IntercambioPorBono> GetIntercambioPorBonos()
        {
            return _context.IntercambioPorBonos.Include(x => x.Bono).Include(x => x.Intercambio);
        }

        // GET: api/IntercambioPorBonos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIntercambioPorBono([FromRoute] int id, int id2)
        {
            if (!ModelState.IsValid)
            {
                // No tengo idea de si esto funcione, no se como agregar la segunda clave foranea
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var IntercambioPorBono = await _context.IntercambioPorBonos.Include(x => x.Bono).Include(x => x.Intercambio).SingleOrDefaultAsync(x => x.IdBono == id && x.IdIntercambio == id2);

            if (IntercambioPorBono == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", IntercambioPorBono.ArticuloSingular, IntercambioPorBono.NombreSingular) });
            }

            return Ok(IntercambioPorBono);
        }

        // PUT: api/IntercambioPorBonos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIntercambioPorBono([FromRoute] int id, [FromBody] IntercambioPorBono IntercambioPorBono)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != IntercambioPorBono.IdBono)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(IntercambioPorBono).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!IntercambioPorBonoExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", IntercambioPorBono.ArticuloSingular, IntercambioPorBono.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/IntercambioPorBonos
        [HttpPost]
        public async Task<IActionResult> PostIntercambioPorBono([FromBody] IntercambioPorBono IntercambioPorBono)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.IntercambioPorBonos.Add(IntercambioPorBono);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/IntercambioPorBonos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIntercambioPorBono([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var IntercambioPorBono = await _context.IntercambioPorBonos.FindAsync(id);
            if (IntercambioPorBono == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", IntercambioPorBono.ArticuloSingular, IntercambioPorBono.NombreSingular) });

            }

            _context.IntercambioPorBonos.Remove(IntercambioPorBono);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool IntercambioPorBonoExists(int id)
        {
            return _context.IntercambioPorBonos.Any(e => e.IdBono == id);
        }
    }
}