﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class IntercambiosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public IntercambiosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Intercambios
        [HttpGet]
        public IEnumerable<Intercambio> GetIntercambios()
        {
            return _context.Intercambios.Include(x => x.Pago).Include(x => x.Usuario);
        }

        // GET: api/Intercambios/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIntercambio([FromRoute] int id, string id2)
        {
            if (!ModelState.IsValid)
            {
                // No tengo idea de si esto funcione, no se como agregar la segunda clave foranea
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Intercambio = await _context.Intercambios.Include(x => x.Pago).Include(x => x.Usuario).SingleOrDefaultAsync(x => x.IdTipoPago == id && x.IdUsuario == id2);

            if (Intercambio == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Intercambio.ArticuloSingular, Intercambio.NombreSingular) });
            }

            return Ok(Intercambio);
        }

        // PUT: api/Intercambios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIntercambio([FromRoute] int id, [FromBody] Intercambio Intercambio)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != Intercambio.IdTipoPago)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(Intercambio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!IntercambioExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Intercambio.ArticuloSingular, Intercambio.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/Intercambios
        [HttpPost]
        public async Task<IActionResult> PostIntercambio([FromBody] Intercambio Intercambio)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.Intercambios.Add(Intercambio);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/Intercambios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIntercambio([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var Intercambio = await _context.Intercambios.FindAsync(id);
            if (Intercambio == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Intercambio.ArticuloSingular, Intercambio.NombreSingular) });

            }

            _context.Intercambios.Remove(Intercambio);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool IntercambioExists(int id)
        {
            return _context.Intercambios.Any(e => e.IdTipoPago == id);
        }
    }
}