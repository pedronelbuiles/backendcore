﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
        public class TipoProductosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoProductosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TipoProductos
        [HttpGet]
        public IEnumerable<TipoProducto> GetTipoProductos()
        {
            return _context.TipoProductos;
        }

        // GET: api/TipoProductos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTipoProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var TipoProducto = await _context.TipoProductos.FindAsync(id);

            if (TipoProducto == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoProducto.ArticuloSingular, TipoProducto.NombreSingular) });
            }

            return Ok(TipoProducto);
        }

        // PUT: api/TipoProductos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoProducto([FromRoute] int id, [FromBody] TipoProducto TipoProducto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != TipoProducto.IdTipoProducto)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(TipoProducto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!TipoProductoExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoProducto.ArticuloSingular, TipoProducto.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/TipoProductos
        [HttpPost]
        public async Task<IActionResult> PostTipoProducto([FromBody] TipoProducto TipoProducto)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.TipoProductos.Add(TipoProducto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/TipoProductos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTipoProducto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var TipoProducto = await _context.TipoProductos.FindAsync(id);
            if (TipoProducto == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoProducto.ArticuloSingular, TipoProducto.NombreSingular) });

            }

            _context.TipoProductos.Remove(TipoProducto);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool TipoProductoExists(int id)
        {
            return _context.TipoProductos.Any(e => e.IdTipoProducto == id);
        }
    }

}