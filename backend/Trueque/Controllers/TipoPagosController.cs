﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
            public class TipoPagosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoPagosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TipoPagos
        [HttpGet]
        public IEnumerable<TipoPago> GetTipoPagos()
        {
            return _context.TipoPagos;
        }

        // GET: api/TipoPagos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTipoPago([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var TipoPago = await _context.TipoPagos.FindAsync(id);

            if (TipoPago == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoPago.ArticuloSingular, TipoPago.NombreSingular) });
            }

            return Ok(TipoPago);
        }

        // PUT: api/TipoPagos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoPago([FromRoute] int id, [FromBody] TipoPago TipoPago)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != TipoPago.IdTipoPago)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(TipoPago).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!TipoPagoExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoPago.ArticuloSingular, TipoPago.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/TipoPagos
        [HttpPost]
        public async Task<IActionResult> PostTipoPago([FromBody] TipoPago TipoPago)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.TipoPagos.Add(TipoPago);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/TipoPagos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTipoPago([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var TipoPago = await _context.TipoPagos.FindAsync(id);
            if (TipoPago == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", TipoPago.ArticuloSingular, TipoPago.NombreSingular) });

            }

            _context.TipoPagos.Remove(TipoPago);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool TipoPagoExists(int id)
        {
            return _context.TipoPagos.Any(e => e.IdTipoPago == id);
        }
    }

}