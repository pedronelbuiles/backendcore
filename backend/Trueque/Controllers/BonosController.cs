﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class BonosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BonosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Bonos
        [HttpGet]
        public IEnumerable<Bono> GetBonos()
        {
            return _context.Bonos.Include(x => x.Producto);
        }

        // GET: api/Bonos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBono([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Bono = await _context.Bonos.Include(x => x.Producto).SingleOrDefaultAsync(x => x.IdBono == id);

            if (Bono == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Bono.ArticuloSingular, Bono.NombreSingular) });
            }

            return Ok(Bono);
        }

        // PUT: api/Bonos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBono([FromRoute] int id, [FromBody] Bono Bono)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != Bono.IdBono)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(Bono).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!BonoExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Bono.ArticuloSingular, Bono.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/Bonos
        [HttpPost]
        public async Task<IActionResult> PostBono([FromBody] Bono Bono)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.Bonos.Add(Bono);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/Bonos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBono([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var Bono = await _context.Bonos.FindAsync(id);
            if (Bono == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Bono.ArticuloSingular, Bono.NombreSingular) });

            }

            _context.Bonos.Remove(Bono);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool BonoExists(int id)
        {
            return _context.Bonos.Any(e => e.IdBono == id);
        }
    }
}