﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clsEntidades;
using clsEntidades.Context;
using Newtonsoft.Json;

namespace Trueque.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class InventariosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public InventariosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Inventarios
        [HttpGet]
        public IEnumerable<Inventario> GetInventarios()
        {
            return _context.Inventarios.Include(x => x.Producto);
        }

        // GET: api/Inventarios/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            var Inventario = await _context.Inventarios.Include(x => x.Producto).SingleOrDefaultAsync(x => x.IdInventario == id);

            if (Inventario == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Inventario.ArticuloSingular, Inventario.NombreSingular) });
            }

            return Ok(Inventario);
        }

        // PUT: api/Inventarios/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventario([FromRoute] int id, [FromBody] Inventario Inventario)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });
            }

            if (id != Inventario.IdInventario)
            {
                return Json(new { success = false, message = string.Format("No se puede editar el identificador principal del registro.") });
            }

            _context.Entry(Inventario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                if (!InventarioExists(id))
                {
                    return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Inventario.ArticuloSingular, Inventario.NombreSingular) });
                }
                else
                {
                    return Json(new { success = false, message = string.Format("Error inesperado al editar el registro." + e.Message) });
                }
            }

            return Json(new { success = false, message = string.Format("El registro se ha editado correctamente.") });

        }

        // POST: api/Inventarios
        [HttpPost]
        public async Task<IActionResult> PostInventario([FromBody] Inventario Inventario)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            _context.Inventarios.Add(Inventario);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar guardar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha guardado correctamente.") });

        }

        // DELETE: api/Inventarios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventario([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = JsonConvert.SerializeObject(new BadRequestObjectResult(ModelState).Value) });

            }

            var Inventario = await _context.Inventarios.FindAsync(id);
            if (Inventario == null)
            {
                return Json(new { success = false, message = string.Format("{0} {1} no se ha encontrado.", Inventario.ArticuloSingular, Inventario.NombreSingular) });

            }

            _context.Inventarios.Remove(Inventario);
            try
            {
                await _context.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return Json(new { success = false, message = string.Format("Ocurrio un error al intentar eliminar el registro en la base de datos." + e.Message) });

            }

            return Json(new { success = false, message = string.Format("El registro se ha eliminar correctamente.") });

        }

        private bool InventarioExists(int id)
        {
            return _context.Inventarios.Any(e => e.IdInventario == id);
        }
    }
}