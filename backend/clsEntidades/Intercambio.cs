﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace clsEntidades
{
    public class Intercambio
    {

        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string ArticuloPlural { get { return "Los"; } }
        public static string NombreSingular { get { return "Intercambio"; } }
        public static string NombrePlural { get { return "Intercambios"; } }


        [Key]
        public int IdIntercambio { get; set; }

        [Required, DisplayFormat(DataFormatString = "DD/MM/YYYY")]
        public string FechaCambio { get; set; }

        [Required]
        public double ValorIntercambio { get; set; }


        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        //Se coloca el nombre del objeto o tabla instanciada.
        [ForeignKey("Usuario")]
        public string IdUsuario { get; set; }

        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        //Se coloca el nombre del objeto o tabla instanciada.
        [ForeignKey("Pago")]
        public int IdTipoPago { get; set; }

        //Se usa para hacer la foranea con la tabla.
        [JsonIgnore]
        public Usuario Usuario { get; set; }

        [JsonIgnore]
        public TipoPago Pago { get; set; }



    }


}
