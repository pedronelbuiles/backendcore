﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace clsEntidades
{
    public class Usuario
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string ArticuloPlural { get { return "Los"; } }
        public static string NombreSingular { get { return "Usuario"; } }
        public static string NombrePlural { get { return "Usuarios"; } }

        // aqui va los datos de la tabla Usuario  q se genere en base de datos 
        //int no requerido  int? es para guardar nulo el cambia para lo requerido
        //{0} es para no quemar la variable 

        [Key]
        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string IdCliente { get; set; }

        [Required, StringLength(50, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string PrimerNombre { get; set; }

        [StringLength(50, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string SegundoNombre { get; set; }

        [Required, StringLength(50, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string PrimerApellido { get; set; }

        [StringLength(50, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string SegundoApellido { get; set; }

        [Required, EmailAddress(ErrorMessage = "El campo {0} en inválido"), StringLength(100, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string Correo { get; set; }

        [Required,DisplayFormat(DataFormatString ="dd/MM/yyyy")]
        public string FechaNacimiento { get; set; }

        [Required]
        public bool Sexo { get; set; }

        [Required]
        public bool Activo { get; set; }

        //esta linea no la guarda en la base e datos  este linea se muestra opcional
        [NotMapped]
        public string NombreCompleto
        {
            get
            {
                return string.Join(" ", PrimerNombre, SegundoNombre, PrimerApellido, SegundoApellido);
            }
        }
    }
}

