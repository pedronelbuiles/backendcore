﻿using System.ComponentModel.DataAnnotations;
namespace clsEntidades
{
    public class TipoProducto
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string ArticuloPlural { get { return "Los"; } }
        public static string NombreSingular { get { return "Tipo Producto"; } }
        public static string NombrePlural { get { return "Tipo Productos"; } }

        [Key]
        public int IdTipoProducto{ get; set; }

        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string Categoria { get; set; }
    }
}
