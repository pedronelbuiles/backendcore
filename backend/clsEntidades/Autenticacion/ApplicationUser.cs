﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace clsEntidades.Autenticacion
{
    public class ApplicationUser : IdentityUser
    {
        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(100)]
        public string Cargo { get; set; }

        public bool Activo { get; set; }

        [StringLength(25)]
        public string Codigo { get; set; }

        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        //{
        //    // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    // Agregar aquí notificaciones personalizadas de usuario
        //    return userIdentity;
        //}
    }

    public class ApplicationRole : IdentityRole
    {

    }

    public class IdentityUserClaim : IdentityUserClaim<string>
    {
        public object ClaimValue { get; internal set; }
    }

    public class IdentityUserLogin : IdentityUserLogin<string>
    {

    }

    public class IdentityUserRole : IdentityUserRole<string>
    {

    }
}
