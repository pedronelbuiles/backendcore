﻿using clsEntidades.Autenticacion;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace clsEntidades.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        //SE PASA DE ENTIDADES A TABLAS
        //EL nombre de la entidad <entidad> va en singular debido a que se maneja de a una
        //El nombre como va a quedar llamada la tabla en la B.D corresponde a la entidad y va en plural
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<TipoProducto> TipoProductos { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Inventario> Inventarios { get; set; }
        public DbSet<Bono> Bonos { get; set; }
        public DbSet<TipoPago> TipoPagos { get; set; }
        public DbSet<Intercambio> Intercambios { get; set; }
        public DbSet<IntercambioPorBono> IntercambioPorBonos { get; set; }

        //Metodo para implementar composición de llaves foraneas y primarias 'composite primary key'
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Linea necesaria para llamar una clave primaria compuesta
            modelBuilder.Entity<IntercambioPorBono>().HasKey(x => new { x.IdBono, x.IdIntercambio });
            //Linea necesaria para llamar una clave foranea compuesta

            base.OnModelCreating(modelBuilder);

            //Personalizacion de tablas Identity
            //Usuario
            modelBuilder.Entity<ApplicationUser>().ToTable("Usuario").Property(x => x.EmailConfirmed).HasColumnName("EmailConfirmado");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.PasswordHash).HasColumnName("HashContrasena");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.SecurityStamp).HasColumnName("SelloSeguridad");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.PhoneNumber).HasColumnName("NumeroTelefono");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.PhoneNumberConfirmed).HasColumnName("NumeroTelefonoConfirmado");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.TwoFactorEnabled).HasColumnName("AutenticacionDosFactores");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.LockoutEnabled).HasColumnName("BloqueoHabilitado");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.AccessFailedCount).HasColumnName("ContadorAccesosFallidos");
            modelBuilder.Entity<ApplicationUser>().Property(x => x.UserName).HasColumnName("NombreUsuario");
            //Rol
            modelBuilder.Entity<IdentityRole>().ToTable("Rol").Property(x => x.Name).HasColumnName("Nombre");
        }
    }
}
