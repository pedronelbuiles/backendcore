﻿using System.ComponentModel.DataAnnotations;

namespace clsEntidades
{
    public class TipoPago
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string NombreSingular { get { return "Tipo de Pago"; } }

        [Key]
        public int IdTipoPago { get; set; }

        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string ModoPago { get; set; }


    }
}
