﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace clsEntidades
{
    public class IntercambioPorBono
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string NombreSingular { get { return "Intercambio por bono"; } }
        [Key]
        [ForeignKey("Bono")]
        public int IdBono { get; set; }

        [Key]
        [ForeignKey("Intercambio")]
        public int IdIntercambio { get; set; }

        //Llamado a las clases de las que depende
        [JsonIgnore]
        public Bono Bono { get; set; }
        [JsonIgnore]
        public Intercambio Intercambio { get; set; }

    }
}
