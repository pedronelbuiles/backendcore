﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace clsEntidades
{
    public class Producto
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string ArticuloPlural { get { return "Los"; } }
        public static string NombreSingular { get { return "Producto"; } }
        public static string NombrePlural { get { return "Productos"; } }

        // aqui va los datos de la tabla Usuario  q se genere en base de datos 
        //int no requerido  int? es para guardar nulo el cambia para lo requerido
        //{0} es para no quemar la variable 

        [Key]
        public int IdProudcto { get; set; }

        [Required, StringLength(200, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string UrlImagen { get; set; }

        [Required, StringLength(30, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string NombreProducto { get; set; }

        [StringLength(200, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        public string Descripcion { get; set; }

        //opcional
        [Required]
        public int Cantidad { get; set; }

        [Required]
        [ForeignKey("TipoProducto")]
        public int IdTipoProducto { get; set; }

        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        //Se coloca el nombre del objeto o tabla instanciada.
        [ForeignKey("Usuario")]
        public string IdUsuario { get; set; }

        //Se usa para hacer la foranea con la tabla.
        [JsonIgnore]
        public Usuario Usuario { get; set; }

        [JsonIgnore]
        public TipoProducto TipoProducto { get; set; }
    }
}
