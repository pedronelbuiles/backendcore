﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace clsEntidades
{
    public class Bono
    {
        // aqui se maneja  las palabras singulares y plurales para el manejo de no quemar las variables(opcional)
        public static string ArticuloSingular { get { return "El"; } }
        public static string NombreSingular { get { return "Bono"; } }

        // aqui va los datos de la tabla Usuario  q se genere en base de datos 
        //int no requerido  int? es para guardar nulo el cambia para lo requerido
        //{0} es para no quemar la variable 

       
        [Required, StringLength(20, ErrorMessage = "El campo {0} supera la longitud máxima de {1} caracteres.")]
        //Se coloca el nombre del objeto o tabla instanciada.
        [ForeignKey("Producto")]
        public int IdProducto { get; set; }

        [Key]
        [Required]
        public int IdBono { get; set; }

        [Required]
        public double ValorBono { get; set; }


        [Required, DisplayFormat(DataFormatString = "DD/MM/YYYY")]
        public string FechaCreacion { get; set; }

        [JsonIgnore]
        public Producto Producto { get; set; }
    }
}
